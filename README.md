# UploaderUi

#### It's an Angular App that displays the UI by making REST calls to the following Spring Boot Apps ####
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)

### Steps (only testing steps) : ###
* Build a docker image "uploader_ui" using command "sudo docker build -t uploader_ui ."
* Create a container, publish 3000 port of the web server to 8083 of host, using "sudo docker run -d -p 8083:3000/tcp --name uploader_ui uploader_ui".
* The UI can be accessed at "http://localhost:8083". The applications "spring_boot_uploader_job" and "spring_boot_uploader_cand" should also be available so that these links in the UI works.

#### The following projects are part of a system that works together : ####
* [spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)
* [uploader-ui](https://bitbucket.org/rajibpsarma/uploader-ui)

