# Stage 1: create a production build
FROM node:10-alpine as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --prod

# Stage 2: deploy our app to the web server
FROM abhin4v/hastatic:latest
COPY --from=build-step /app/dist/uploader-ui /opt/mywebsite
WORKDIR /opt/mywebsite
CMD ["/usr/bin/hastatic"]
