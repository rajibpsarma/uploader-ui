import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobDetailsComponent } from './details/job-details/job-details.component';
import { CandDetailsComponent } from './details/cand-details/cand-details.component';


const routes: Routes = [
  {path:"jobs", component:JobDetailsComponent},
  {path:"cands", component:CandDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
