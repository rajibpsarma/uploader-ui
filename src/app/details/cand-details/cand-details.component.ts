import { Component, OnInit } from '@angular/core';
import { CandidateService } from 'src/app/services/CandidateService';

@Component({
  selector: 'app-cand-details',
  templateUrl: './cand-details.component.html',
  styleUrls: ['./cand-details.component.css']
})
export class CandDetailsComponent implements OnInit {
  errorMsg : string;
  isLoading = true;
  candDetails = {"candidatesUploaded":"1", "totalCandidates":"100", "startDate":"30/10/2020", "pausedDate":"07/08/2020"};
  constructor(private candService : CandidateService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.errorMsg = "";
    this.candService.getCandidateDetails().subscribe(
      (data : {"candidatesUploaded":"1", "totalCandidates":"100", "startDate":"xyz", "pausedDate":"def"}) => {
        console.log("data fetched");
        this.candDetails = data;
        this.isLoading = false;
      },
      (error) => {
        this.errorMsg = "An error occured while fetching Candidate details."
        this.isLoading = false;
      }
    );
  }
}
