import { Component, OnInit } from '@angular/core';
import { JobService } from 'src/app/services/JobService';
//import { JobService } from "../../services/JobService";

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit {
  errorMsg : string;
  isLoading = true;
  jobDetails : {jobsUploaded, totalJobs, startDate, pausedDate};
  //jobDetails = {"jobsUploaded":"1", "totalJobs":"100", "startDate":"30/10/2020", "pausedDate":"07/08/2020"};
  constructor(private jobService : JobService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.errorMsg = "";
    this.jobService.getJobDetails().subscribe(
      (data : {"jobsUploaded":"1", "totalJobs":"2", "startDate":"xyz", "pausedDate":"def"}) => {
        console.log("data fetched");
        this.jobDetails = data;
        this.isLoading = false;
      },
      (error) => {
        console.log("error");
        this.errorMsg = "An error occured while fetching Job details."
        this.isLoading = false;
      }
    );
  }

}
