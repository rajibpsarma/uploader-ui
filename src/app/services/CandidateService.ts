import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({providedIn:"root"})
export class CandidateService {

  constructor(private http : HttpClient) {
  }

  getCandidateDetails() {
    //return this.http.get("http://localhost:8080/spring_boot_uploader_cand/rest/candidates");
	return this.http.get("http://localhost:8082/rest/candidates");
  }
}
