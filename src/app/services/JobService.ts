import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({providedIn:"root"})
export class JobService {

  constructor(private http : HttpClient) {
  }

  getJobDetails() {
    //return this.http.get("http://localhost:8080/spring_boot_uploader_job/rest/jobs");
	return this.http.get("http://localhost:8081/rest/jobs");

  }
}
